// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
// const module = require('module');

// Local Requirements.
// const module = require('./module');

// Global Variables.
var globalVar;

/**
 * This class provides methods to .....
 */
module.exports = function(bot, options) {
  logger.info(`${THISFILE}: Plugin Attached To Bot.`)
  // Get all options.
  let PREFIX = (options && options.prefix) || ';';

  bot.on('message', msg => {
    // Cleans up any white space.
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'cmd1':
          return cmd1_function(msg, suffix);
        case 'cmd2':
          return cmd2_function(msg, suffix);
        case 'cmd3':
          return cmd3_function(msg, suffix);
      }
    }
  });

  function cmd1_function(msg, suffix) {
  }

  function cmd2_function(msg, suffix) {
  }

  function cmd3_function(msg, suffix) {
  }

}
