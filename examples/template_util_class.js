// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
// const module = require('module');

// Local Requirements.
// const module = require('./module');

// Global Variables.
var globalVar;

/**
 * This class provides methods to .....
 */
module.exports = class CLASSName {
  constructor() {
    logger.debug(`${THISFILE}: Constructor Initialized.`)
  }

  classMethod1() {}

  classMethod2() {}
}

function helperMethod() {

}
