// Sets up testing framework.
const assert = require('assert');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

// Turn off winston's logging.
const logger = require('./logging/manage/loggingManager.js');
logger.disable();

// Import JSON config.
const fs = require('fs-extra');
const config = JSON.parse(fs.readFileSync('./config/config.json', 'utf8'));

/**
 * Runs the following test. Comment those you want to omit.
 *
 * @return {Mocha} We are using mocha for our assertions.
 */
(function runTests() {
  testHelper();
  testCAS();
  testNoctools();
  testPolycom();
})();

/**
 * This testcase is used to test the polycom utilities.
 *
 * @return {[type]} [description]
 */
function testPolycom() {

  // Our Polycom Units
  const POLY = config['POLYCOM'];
  const unit1208 = POLY.unit1;
  const unit1218 = POLY.unit2;
  const unit2208 = POLY.unit3;
  const all = POLY.allunits;
  const pword = POLY.password;

  const Polycom = require('./util/polycom');
  var poly = new Polycom(pword, all);

  describe('Polycom', function() {
    this.timeout(20000); // Max time individual tests allowed to run.
    describe('#getCallState()', function() {
      it('Test Case: Check for basic telnet functions.', function() {
        return poly.getCallState(unit1218)
          .then(function(result) {
            var testResult = poly.getTestResults();
            expect(testResult).to.equal(true);
          })
      });
    });
    describe('#whoami()', function() {
      it('Test Case: Check for basic telnet functions.', function() {
        return poly.whoami(unit1218)
          .then(function(result) {
            var testResult = poly.getTestResults();
            expect(testResult).to.equal(true);
          })
      });
    });
  });
}

/**
 * This testcase is used to test the helper utilities.
 *
 * @return {[type]} [description]
 */
function testHelper() {
  const Helper = require('./util/helper.js');
  const helper = new Helper();

  describe('Helpers', function() {
    describe('#toTime()', function() {
      it('Test Case 1 - Should return 5:23:15', function() {
        assert.equal(helper.toTime(19395000), "5:23:15");
      });
    });
  });
}

/**
 * This test case is used to test the noctools utilities. The following codes
 * are used to determine what each case is testing for.
 *
 * Possible Test Results
 * {'001'} = Simple report returned
 * {'002'} = Detailed report returned
 *
 * @return {[type]} [description]
 */
function testNoctools() {
  const Noctools = require('./util/noctools.js');
  const noctools = new Noctools();
  const GENERAL = config['GENERAL'];

  describe('Noctools', function() {
    this.timeout(20000); // Max time individual tests allowed to run.
    describe('#query()', function() {
      it('Test Case: 1 - Check if Simple report returned', function() {
        return noctools.query(config['CAS_CRED'].username,
          config['CAS_CRED'].password, 0, GENERAL.test_ipaddress)
          .then(function(result) {
            var testResult = noctools.getTestResults();
            expect(testResult).to.equal('001');
          })
      });
      it('Test Case: 2 - Check if Detailed report returned', function() {
        return noctools.query(config['CAS_CRED'].username,
          config['CAS_CRED'].password, 1, GENERAL.test_ipaddress)
          .then(function(result) {
            var testResult = noctools.getTestResults();
            expect(testResult).to.equal('002');
          })
      });
    });
  });
}

/**
 * This test case is used to test the CAS utilities. The following codes
 * are used to determine what each case is testing for.
 *
 * Possible Test Results
 * {true} = Successful connection.
 * {false} = Unsuccessful connection.
 *
 * @return {[type]} [description]
 */
function testCAS() {
  const CASAuth = require('./util/casauth.js');
  const CAS = new CASAuth();

  describe('CAS', function() {
    this.timeout(20000); // Max time individual tests allowed to run.
    describe('#casAuthNoc()', function() {
      it('Test Case: 1 - Check if successful connection.', function() {
        return CAS.casAuthNoc(config['CAS_CRED'].username,
          config['CAS_CRED'].password)
          .then(function(result) {
            var testResult = CAS.getTestResults();
            expect(testResult).to.equal(true);
          })
      });
      it('Test Case: 2 - Check if unsuccessful connection.', function() {
        return expect(CAS.casAuthNoc('fake', 'credentials'))
          .to.be.rejectedWith(false);
      });
    });
    describe('#casAuth()', function() {
      it('Test Case: 1 - Check if successful connection.', function() {
        return CAS.casAuth(config['CAS_CRED'].username,
          config['CAS_CRED'].password, config['CAS_CRED'].url)
          .then(function(result) {
            var testResult = CAS.getTestResults();
            expect(testResult).to.equal(true);
          })
      });
      it('Test Case: 2 - Check if unsuccessful connection.', function() {
        return expect(CAS.casAuth('fake', 'credentials',
          config['CAS_CRED'].url))
          .to.be.rejectedWith(false);
      });
    });
  });
}
