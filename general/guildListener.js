// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();


// Local Requirements.
const Helper = require('../util/helper.js');

/**
 * Adds a guildListener plugin to your bot.
 *
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  logger.info(`${THISFILE}: Plugin Attached To Bot.`)

  let PREFIX = (options && options.prefix) || ';';

  bot.on("guildCreate", function(server) {
    console.log("Trying to insert server " + server.name + " into database");
    var info = {
      "servername": "'" + server.name + "'",
      "serverid": server.id,
      "ownerid": server.owner.id,
      "prefix": PREFIX
    }

    connection.query("INSERT INTO servers SET ?", info, function(error) {
      if (error) {
        console.log(error);
        return;
      }
      console.log("Server Inserted to servers!");
    });
  });

  bot.on("guildDelete", function(server) {
    console.log("Attempting to remove " + server.name + " from the database");

    connection.query("DELETE FROM servers WHERE serverid = '" + server.id + "'", function(error) {
      if (error) {
        console.log(error)
        return;
      }
      console.log("Server Removed From servers!")
    })
  });

  bot.on("guildMemberAdd", (server, member) => {
    console.log(`New User "${member.user.username}" has joined "${server.name}"`);
    server.defaultChannel.sendMessage(`${member.user} has **joined** the server`);
  });

  bot.on('guildMemberRemove', (server, member) => {
    console.log(`User "${member.user.username}" has been kicked from "${server.name}"`);
    server.defaultChannel.sendMessage(`${member.user} has been **booted**`);
  });

  bot.on('guildBanAdd', (server, member) => {
    server.defaultChannel.sendMessage(`Someone **Banned** ${member} Cash him/her outside`);
  });

  bot.on('guildBanRemove', (server, member) => {
    server.defaultChannel.sendMessage(`${member} has had his **Ban** lifted. How bah dah`);
  });
}
