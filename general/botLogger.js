// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

/**
 * Always include this logger!
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  logger.info(`${THISFILE}: Plugin Attached To Bot.`)

  let PREFIX = (options && options.prefix) || ';';

  bot.on('ready', () => {
    logger.info(`${THISFILE}: I am ready! and currently running on ${bot.guilds.size} servers`);
    bot.user.setStatus('status', `Message ${PREFIX}help`)
      .then(user => logger.info(`${THISFILE}: Set status!`))
      .catch(function(err) {
        logger.error(`${THISFILE}: ${err}`);
      });
  });

  bot.on('error', e => {
    logger.error(`${THISFILE}: ${e}`);
  });

  bot.on('warn', w => {
    logger.warn(`${THISFILE}: ${w}`);
  });

  bot.on('debug', d => {
    logger.silly(`${THISFILE}: ${d}`);
  });

  process.on('exit', (code) => {
    logger.error(`${THISFILE}: EXIT: ${code}`);
  });

  process.on('uncaughtException', (ex) => {
    if (ex.code == 'ECONNRESET') {
      logger.error(`${THISFILE}: Bot Caught : ${ex}`);
      return;
    } else {
      logger.error(`${THISFILE}: Bot Caught : ${ex}`);
    }
  });

}
