// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// Local Requirements.
const Helper = require('../util/helper.js')

// Global Variables.
var helper = new Helper();

/**
 * Adds general features to your bot.
 * @param  {Client} bot     This is the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  logger.info(`${THISFILE}: Plugin Attached To Bot.`)

  // Get all options.
  let PREFIX = (options && options.prefix) || ';';

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'ping':
        case 'pingbot':
          return ping(msg, suffix);
        case 'avatar':
          return avatar(msg, suffix);
        case 'purge':
          return purge(msg, suffix);
        case 'help':
          return help(msg, suffix);
        case 'listguilds':
          return listguilds(msg, suffix);
        case 'serverinfo':
          return serverinfo(msg, suffix);
        case 'shutdown':
          return shutdown(msg, suffix);
      }
    }
  });

  function help(msg, suffix) {
    msg.author.sendMessage(`Hello, my name is ** ${bot.user}**!
  I was invited to your server **${msg.channel.guild.name}** by the server owner.

  __**General Commands**__
  ${PREFIX}serverinfo
  ${PREFIX}ping
  ${PREFIX}purge

  __**Basic Commands**__
  ${PREFIX}help
  ${PREFIX}avatar
  `);
  }

  /**
   * Returns information about the server the message was sent in.
   * @param  {[type]} msg    [description]
   * @param  {[type]} suffix [description]
   * @return {[type]}        [description]
   */
  function serverinfo(msg, suffix) {
    if (!isAdmin(msg.author)) {
      msg.channel.sendMessage(`Not admin`)
    }

    msg.author.sendMessage(`**Server Information**
    Server Name: ${msg.channel.guild.name}
    Server ID: ${msg.channel.guild.id}
    Server Owner: ${msg.channel.guild.owner.user}
    Server Created: ${msg.channel.guild.createdAt.toUTCString()}`);
  }

  function avatar(msg, suffix) {
    let mentioneduser = msg.mentions.users.first();
    let userOG = msg.author;
    let aURL;

    if (mentioneduser === undefined) {
      aURL = userOG.avatarURL;
    } else {
      aURL = mentioneduser.avatarURL;
    }
    if (aURL === null) return msg.channel.sendMessage(`**User has no avatar URL**`);
    msg.channel.sendMessage(aURL);
  }

  function listguilds(msg, suffix) {
    if (!isAdmin(msg.author)) {
      msg.channel.sendMessage(`Not **admin**`)
    }

    let guildList = bot.guilds.array();
    for (i = 0; i < guildList.length; i++) {
      msg.channel.sendMessage(`I am currently in the following guilds

        **${i + 1}. ${guildList[i].name}**`);
    }
  }

  function purge(msg, suffix) {
    let number = suffix;
    var num = 99;
    num = parseInt(number) + 1;

    msg.channel.fetchMessages({
      limit: num
    }).then(messages => {
      msg.channel.bulkDelete(messages)
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`)
        });
      msg.channel.sendMessage(`Removed ${messages.size - 1} Messages`)
        .then(newMessage => newMessage.delete(3000))
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`)
        });
    });
  }

  function ping(msg, suffix) {
    if (!isAdmin(msg.author)) {
      msg.channel.sendMessage(`Not **admin**`)
    }

    msg.channel.sendMessage('Hi admin, ' + msg.author + ', I am ' +
      bot.user + "\n" + "You are on **" + msg.channel.guild.name + "**" +
      "\n" + " I have been online for " + helper.toTime(bot.uptime));
  }

  function shutdown(msg, suffix) {
    if (!isAdmin(msg.author)) {
      msg.channel.sendMessage(`Not **admin**`)
    }

    msg.channel.sendMessage(`**See ya later, Bot shutting down...**`);
    bot.destroy();
    setTimeout(() => {
      process.exit();
    }, 1000);
  }

  // This should be a global function possibly in config.
  function isAdmin(admin) {
    let adminBool = false;
    if (admin.id == 170702846260412416 || admin.id == 255414300858908674) {
      adminBool = true;
    }
    return adminBool;
  }

}
