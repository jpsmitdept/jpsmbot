# README #

This bot was created to be used with discord in order to simplify
life at JPSM

### What is this repository for? ###

* Integrate Discord bot with many services from the University of MD.
* v1.0.1

### Who do I talk to? ###

* Ed Fabre <enfabre@umd.edu>
* Ricky Sokel <rlsokel@umd.edu>
* IT-Support <jpsm-itsupport@umd.edu>

### How To Use ###

* Configure the config-example.json file to cater to your department. Afterwards remove the '-example' from both the json file and it's parent folder.
