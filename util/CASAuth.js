// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const selenium = require('selenium-webdriver');
const phantomjs_exe = require('phantomjs-bin').path;

// Global Variables.
var customPhantom = selenium.Capabilities.phantomjs()
  .set("phantomjs.binary.path", phantomjs_exe);
var driver;
var By = selenium.By;
var mainHandle = null;
var testResults = null;

/**
 * This class provides methods to authenticate with CAS.
 */
module.exports = class CASAuth {
  constructor() {
    logger.debug(`${THISFILE}: Constructor Initialized.`)
  }

  getMainHandle() {
    return new Promise(function(resolve, reject) {
      resolve(driver.getWindowHandle());
    });
  }

  // class methods
  getDriver() {
    return driver;
  }

  /**
   * Attempts authentication with CAS to connect to Network Managers IP resolution
   * area. Returns true or false.
   *
   * @param  {String} user User with LAN Admin Status.
   * @param  {String} pass
   * @return {boolean}      Returns true or false depending on success.
   */
  casAuthNoc(user, pass) {

    return new Promise(function(resolve, reject) {
      // Connects to url
      driver = new selenium.Builder().withCapabilities(customPhantom).build();

      driver.get("https://noc.net.umd.edu/cgi-bin/netmgr/main");
      driver.getCurrentUrl().then(function(u) {
        logger.info(`${THISFILE}: Attempting to connect to ${u}`);
      });
      /* Finds textbox elements and CAS login Button */
      var username = driver.findElement(By.name("username"));
      var password = driver.findElement(By.name("password"));
      var button = driver.findElement(By.name("submit"));

      username.clear();
      username.sendKeys(user);
      password.clear();
      password.sendKeys(pass);
      button.click();

      driver.getPageSource().then(function(src) {
        var stringFromSuccessPage = "You can also enter a MAC address to " +
          "see which IP addresses that MAC\n" + "address has used ";
        if (src.includes(stringFromSuccessPage)) {
          logger.info(`${THISFILE}: Login through CAS Succesful`);
          driver.getWindowHandle().then(function(handle) {
            logger.debug(`${THISFILE}: Current Window handle is - ${handle}`);
            mainHandle = handle;
          });
          testResults = true;
          resolve(true);
        } else {
          logger.warn(`${THISFILE}: FAILED Login, check your credentials.`);
          testResults = false;
          reject(false);
        }
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
          reject(false);
        });
    });
  }

  /**
   * Attempts authentication with CAS to connect to CAS.
   *
   * @param  {String} user User with LAN Admin Status.
   * @param  {String} pass
   * @return {boolean}      Returns true or false depending on success.
   */
  casAuth(user, pass, url) {

    return new Promise(function(resolve, reject) {
      // Connects to url
      driver = new selenium.Builder().withCapabilities(customPhantom).build();

      driver.get(url);
      driver.getCurrentUrl().then(function(u) {
        logger.info(`${THISFILE}: Attempting to connect to ${u}`);
      });
      /* Finds textbox elements and CAS login Button */
      var username = driver.findElement(By.name("username"));
      var password = driver.findElement(By.name("password"));
      var button = driver.findElement(By.name("submit"));

      username.clear();
      username.sendKeys(user);
      password.clear();
      password.sendKeys(pass);
      button.click();

      driver.getPageSource().then(function(src) {
        var stringFromSuccessPage = "Log In Successful";
        if (src.includes(stringFromSuccessPage)) {
          logger.info(`${THISFILE}: Login through CAS Succesful`);
          driver.getWindowHandle().then(function(handle) {
            logger.debug(`${THISFILE}: Current Window handle is - ${handle}`);
            mainHandle = handle;
          });
          testResults = true;
          resolve(true);
        } else {
          logger.warn(`${THISFILE}: FAILED Login, check your credentials.`);
          testResults = false;
          reject(false);
        }
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
          reject(false);
        });
    });
  }

  getTestResults() {
    return testResults;
  }
}
