// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const Telnet = require('telnet-client');
const Repeat = require('repeat');
const fs = require('fs-extra');

// Global Variables.
var pword;
var polyUnits = [];
var testResults = null;
var ID = 0;

/**
 * This class provides methods to interface with polycom HDX units.
 */
module.exports = class Polycom {
  constructor(p, all) {
    logger.debug(`${THISFILE}: Constructor initializing`);
    pword = p;
    // Our Polycom Units

    for (var i = 0; i < all.length; i++) {
      polyUnits.push(all[i]);
    }
    logger.debug(`${THISFILE}: ${polyUnits.length} polycom units initialized: ${polyUnits}`);
  }

  dial(host, dest) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function dial() was called.`);

      testResults = null;
      var d = `dial auto ${dest}`;
      var s = `getcallstate`;
      sendCommand(d, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function dial() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  getCallState(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function getCallState() was called.`);

      testResults = null;
      var cmd = `getcallstate`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function getCallState() executed successfully.`);
        resolve(dialSuccess(res));
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  getSN(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function getSN() was called.`);

      testResults = null;
      var cmd = `serialnum`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function getSN() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  getSystemName(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function getSystemName() was called.`);

      testResults = null;
      var cmd = `systemname get`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function getSystemName() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  getVersion(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function getVersion() was called.`);

      testResults = null;
      var cmd = `version`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function getVersion() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  hangup(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function hangup() was called.`);

      testResults = null;
      var cmd = `hangup all`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function hangup() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  mpAutoAnswer(opt, host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function mpAutoAnswer() was called.`);

      testResults = null;
      var cmd = `mpautoanswer ${opt}`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function mpAutoAnswer() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  ppAutoAnswer(opt, host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function ppAutoAnswer() was called.`);

      testResults = null;
      var cmd = `autoanswer ${opt}`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function ppAutoAnswer() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  setCamInstructor(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function setCamInstructor() was called.`);

      testResults = null;
      var cmd = `preset near go 2`
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function setCamInstructor() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  setCamStudents(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function setCamStudents() was called.`);

      testResults = null;
      var cmd = `preset near go 1`
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function setCamStudents() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  restart(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function restart() was called.`);

      testResults = null;
      var cmd = `reboot now`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function restart() executed successfully.`);
        resolve(`Restart Command Sent`);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  restartAll() {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function restartAll() was called.`);

      testResults = null;
      for (var i = 0; i < polyUnits.length; i++) {
        var cmd = `reboot now`;
        sendCommand(cmd, polyUnits[i]).then(function(res) {
          testResults = true;
          if (i == polyUnits.length - 1) {
            logger.debug(`${THISFILE}: The function restartAll() executed successfully.`);
            resolve(`Restart All Command Sent`);
          }
        })
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      }
    });
  }

  whoami(host) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: The function whoami() was called.`);
      testResults = null;
      var cmd = `whoami`;
      sendCommand(cmd, host).then(function(res) {
        testResults = true;
        logger.debug(`${THISFILE}: The function whoami() executed successfully.`);
        resolve(res);
      })
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    });
  }

  getTestResults() {
    return testResults;
  }
}

/**
 * Creates a Session ID for the instances.
 * @return {[type]} [description]
 */
function getSessionID() {
  return new Promise(function(resolve, reject) {
    var sessionID;
    if (ID === 0) {
      sessionID = ID;
      ID++;
    } else {
      sessionID = 0;
      while (sessionID < ID) {
        sessionID++;
      }
      ID++;
    }
    resolve(sessionID);
  });
}

function dialSuccess(packet) {
  var callSuccess = false;
  logger.debug(`${THISFILE}: Checking if call was successful`);
  packet = packet.split('cs: call[');
  packet.shift();
  for (var i = 0; i < packet.length; i++) {
    if (packet[i].includes('connected')) {
      logger.debug(`${THISFILE}: Call socket[${i}] connected.`);
      callSuccess = true;
      var speed = packet[0].substring(packet[0].indexOf('speed[') + 6);
      var dialstr = packet[0].substring(packet[0].indexOf('dialstr[') + 8);
      var state = packet[0].substring(packet[0].indexOf('state[') + 6);

      speed = speed.substring(0, speed.indexOf(']'));
      dialstr = dialstr.substring(0, dialstr.indexOf(']'));
      state = state.substring(0, state.indexOf(']'));

      packet[i] = {
        state: state,
        speed: speed,
        dialstr: dialstr
      }
    } else {
      logger.debug(`${THISFILE}: Call socket[${i}] disconnected.`);
      packet[i] = {
        state: "disconnected",
        speed: "none",
        dialstr: "none"
      }
    }
  }

  if (callSuccess) {
    logger.debug(`${THISFILE}: Call was successful`);
  } else {
    logger.debug(`${THISFILE}: Call was not successful`);
  }
  return packet;
}

/**
 * Sends the Polycom API command to the appropriate HDX unit.
 *
 * @param  {String} cmd  Valid telnet command for Polycom API.
 * @param  {String} host IP address or dns of desired HDX unit.
 * @return {String}      Returns a string based on output of the command.
 */
function sendCommand(cmd, host) {
  var sessionID;
  getSessionID().then(function(id) {
    sessionID = `SID:${id} | `;
  });

  return new Promise(function(resolve, reject) {
    var connection = new Telnet();
    var params = {
      host: `${host}`,
      port: 24,
      shellPrompt: "Welcome to ViewStation",
      maxBufferLength: 50000,
      timeout: 3000,
    };

    connection.on('connect', function() {
      logger.info(`${THISFILE}: ${sessionID}Telnet session opened with ${host}`);
    })

    connection.on('ready', function(prompt) {
      connection.send(pword, function(err, res) {
        logger.info(`${THISFILE}: ${sessionID}Telnet succesfully entered password.`);
        connection.send(cmd, function(e, r) {
          resolve(r);
        });
      });
    });

    connection.on('timeout', function() {
      logger.info(`${THISFILE}: ${sessionID}Telnet socket with ${host} timed out, closing...`)
      connection.end();
    });

    connection.on('close', function() {
      logger.info(`${THISFILE}: ${sessionID}Telnet session with ${host} closed.`);
    });

    connection.connect(params);
  });
}
