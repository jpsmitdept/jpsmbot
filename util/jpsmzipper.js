// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const fs = require("fs-extra");
const archiver = require('archiver');

/**
 * This class is provides methods to zip files and folders.
 */
module.exports = class JPSMZipper {
  constructor() {
    logger.debug(`${THISFILE}: Constructor Initialized.`)
  }

  /**
   * Zip a directory recursively to desired location.
   *
   * @param  {String} dir [description]
   * @param  {String} out [description]
   * @return {boolean}    [description]
   */
  directory(dir, out) {
    return new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: Attempting to zip "${dir}" to "${out}"`);
      var output = fs.createWriteStream(out);
      var archive = archiver('zip');
      archive.pipe(output);
      archive.bulk([{
        expand: true,
        cwd: dir,
        src: ['**'],
      }]);
      archive.finalize();

      output.on('close', function() {
        logger.debug(`${THISFILE}: ${archive.pointer()} total bytes`);
        logger.debug(`${THISFILE}: Zipper instance closed.`);
        resolve(true);
      });

      archive.on('error', function(err) {
        logger.error(`${THISFILE}: ${err}`);
        reject(false);
      });
    });
  }

  /**
   * Zip a single file to desired location.
   *
   * @param  {String} pToFile        [description]
   * @param  {String} out [description]
   * @return {boolean}                   [description]
   */
  file(pToFile, out) {
    new Promise(function(resolve, reject) {
      logger.debug(`${THISFILE}: Attempting to zip "${pToFile}" to "${out}"...`);
      var output = fs.createWriteStream(out);
      var archive = archiver('zip');
      archive.pipe(output);
      archive.file(pToFile);
      archive.finalize();

      output.on('close', function() {
        logger.debug(`${THISFILE}: ${archive.pointer()} total bytes`);
        logger.debug(`${THISFILE}: Zipper instance closed.`);
        resolve(true);
      });

      archive.on('error', function(err) {
        logger.error(`${THISFILE}: ${err}`);
        reject(false);
      });
    });
  }
}
