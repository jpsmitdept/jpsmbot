// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements
const slack = require('slack-incoming-webhook');

// Global Variables.
var slackHook;

/**
 * This class is used to send messages to the Slack incoming webhook.
 */
module.exports = class Slack {
  constructor(hook) {
    slackHook = hook;
    logger.debug(`${THISFILE}: Object initialized with hook: ${slackHook}`);
  }

  send(message) {
    var sendMessage = slack({
      url: slackHook,
    });
    sendMessage(message);
    logger.info(`${THISFILE}: Message sent to slack!`);
  }
}
