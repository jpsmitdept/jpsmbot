// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const MailListener = require("mail-listener2");
const fs = require('fs-extra');

// Global Variables.
var mailListener;
var tempInbox = [];

/**
 * This class provides an imap listener to grab emails to be read by discord.
 */
module.exports = class JPSMMailListener {
  constructor(user, pass, imapHost, imapPort, inbox, attachmentsOp) {
    logger.debug(`${THISFILE}: Constructor Initialized.`);
    tempInbox.hasNewMail = false;
    tempInbox.hasAttachments = false;

    mailListener = new MailListener({
      username: user,
      password: pass,
      host: imapHost,
      port: imapPort, // imap port
      tls: true,
      connTimeout: 10000, // Default by node-imap
      authTimeout: 5000, // Default by node-imap,
      // debug: console.log, // Or your custom function with only one incoming argument. Default: null
      tlsOptions: {
        rejectUnauthorized: false
      },
      mailbox: inbox, // mailbox to monitor
      // searchFilter: ["SEEN"], // the search filter being used after an IDLE notification has been retrieved
      markSeen: true, // all fetched email willbe marked as seen and not fetched next time
      fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
      mailParserOptions: {
        streamAttachments: false
      }, // options to be passed to mailParser lib.
      attachments: attachmentsOp, // download attachments as they are encountered to the project directory
      attachmentOptions: {
        directory: "./util/attachments/"
      } // specify a download directory for attachments
    });
    mailListener.start();

    mailListener.on("server:connected", function() {
      logger.info(`${THISFILE}: IMAP Server Connected`);
    });

    mailListener.on("server:disconnected", function() {
      logger.info(`${THISFILE}: IMAP Server Disconnected`);
    });

    mailListener.on("error", function(err) {
      logger.error(`${THISFILE}: ${err}`);
    });

    mailListener.on("mail", function(mail, seqno, attributes) {
      // Switch new mail to true.
      tempInbox.hasNewMail = true;
      var attachNames = [];
      var attDir;


      if (mail.attachments !== undefined) {
        logger.info(`${THISFILE}: New Email Received [ sequence_no: ${seqno}, date: ${attributes.date}, message_length: ${mail.text.length}, num_attachments: ${mail.attachments.length} ]`);
        attDir = `./util/attachments/attachments_${seqno}_${mail.subject}`
        for (var i = 0; i < mail.attachments.length; i++) {
          var attachFileName = mail.attachments[i].fileName;
          attachNames.push(attachFileName);

          // Moves files from default to specific folder
          var source = `./util/attachments/${attachFileName}/`
          var dest = `${attDir}/${attachFileName}`
          fs.move(source, dest, function(err) {
            if (err) {
              return logger.error(`${THISFILE}: Error while moving items`);
            }
            logger.debug(`${THISFILE}: Successfully moved file!`)
          })
        }
        tempInbox.hasAttachments = true;
      } else {
        logger.info(`${THISFILE}: New Email Received [ sequence_no: ${seqno}, date: ${attributes.date}, message_length: ${mail.text.length}, num_attachments: 0 ]`);
        attDir = "";
        tempInbox.hasAttachments = false;
      }

      var discordFormattedMail = {
        'message': `__**NEW MESSAGE:**__
**Subject:** ${mail.subject}
**Date** ${mail.date}
**From:** ${mail.from[0].name} <${mail.from[0].address}>
**To:** ${mail.to[0].name} <${mail.to[0].address}>
**Body:**
${mail.text}`,
        'attachment_dir': `${attDir}`,
        'attachments': `${attachNames}`
      };

      // Create a discord message based on the mail received.
      tempInbox.push(discordFormattedMail);

    });

    mailListener.on("attachment", function(attachment) {
      logger.debug(`${THISFILE}: Attachment Path: ${attachment.path}`);
    });
  }

  startListener() {
    mailListener.start();
  }

  stopListener() {
    mailListener.stop();
  }

  checkMail() {

    if (tempInbox.hasNewMail) {

      // Clone the messages array.
      var cloneMessage = tempInbox.slice(0);
      var cloneHasMail = tempInbox.hasNewMail;

      logger.info(`${THISFILE}: New Messages Received.`);

      // Reset the tempInbox.
      tempInbox.hasNewMail = false;
      tempInbox = [];

      return cloneMessage;
    }
  }
}
