// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const numeral = require('numeral');

/**
 * This class provides basic and general methods.
 */
module.exports = class Helper {
  constructor() {
    logger.debug(`${THISFILE}: Constructor Initialized.`)
  }

  /**
   * Converts MS to neat time format.
   *
   * @param  {Integer} milleseconds
   * @return {[type]}   [description]
   */
  toTime(milleseconds) {
    return numeral((milleseconds / 1000) | 0).format('00:00:00');
  }
}
