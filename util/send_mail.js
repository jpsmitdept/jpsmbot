// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const nodemailer = require('nodemailer');

// Global Variables.
var transporter;
var mailOptions;

/**
 * This class provides methods to .....
 */
module.exports = class Mailer {
  constructor(inService, inUser, inPass) {
    logger.debug(`${THISFILE}: Constructor Initialized.`);

    transporter = nodemailer.createTransport({
      service: inService,
      auth: {
        user: inUser,
        pass: inPass
      }
    });
  }

  send(sub, message, fromName, fromEmail, toEmail) {
    mailOptions = {
      from: `"${fromName}" <${fromEmail}>`, // sender address
      to: toEmail, // list of receivers
      subject: sub, // Subject line
      text: message // plain text body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return logger.error(`${THISFILE}: ${error}`);
      }
      logger.info(`${THISFILE}: Email message sent.`);
    });
  }
}
