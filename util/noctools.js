// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const cheerio = require('cheerio');
const selenium = require('selenium-webdriver');
const phantomjs_exe = require('phantomjs-bin').path;

// Local Requirements.
const CASAuth = require('../util/casauth.js');

// Global Variables.
var cas;
var customPhantom;
var driver;
var By;
var casActive;
var mainHandle;
var testResults;

/**
 * This class provides methods to interface with the network managers area at
 * the University of Maryland.
 */
module.exports = class Noctools {
  constructor() {
    logger.debug(`${THISFILE}: Constructor Initialized.`)
    logger.debug(`${THISFILE}: Phantom JS: ${phantomjs_exe}.`)

    cas = new CASAuth();
    customPhantom = selenium.Capabilities.phantomjs()
      .set("phantomjs.binary.path", phantomjs_exe);
    driver;
    By = selenium.By;
    casActive = false;
    mainHandle = null;
    testResults = null;
  }

  query(inputUser, inputPass, inputChoice, inputAddr) {
    return new Promise(function(resolve, reject) {

      if (!casActive) {
        cas.casAuthNoc(inputUser, inputPass).then(function(res) {
          logger.debug(`${THISFILE}: ${res}`);
          driver = cas.getDriver();
          mainHandle = cas.getMainHandle();
          if (res) {
            casActive = res;
            submitIPMACPromise(inputAddr).then(function(ipCheck) {
              if (ipCheck === true) {
                getSolutionMessage(inputChoice).then(function(r) {
                  resolve(r);
                });
              }
            });
          }
        });
      }
      if (casActive) {
        driver.switchTo().window(mainHandle);

        driver.getPageSource().then(function(src) {
          var stringFromSuccessPage = "You can also enter a MAC address to " +
            "see which IP addresses that MAC\n" + "address has used ";
          if (src.includes(stringFromSuccessPage)) {
            driver.getWindowHandle().then(function(handle) {
              mainHandle = handle;
            });

            logger.debug(`${THISFILE}: Login through CAS Succesful`);

            logger.debug(`${THISFILE}: Jumping back to handle: ${mainHandle}`);
            submitIPMACPromise(inputAddr).then(function(ipCheck) {
              if (ipCheck === true) {
                getSolutionMessage(inputChoice).then(function(r) {
                  resolve(r);
                });
              }
            });
          } else {
            logger.debug(`${THISFILE}: You were logged out, Logging back in.`);
            cas.casAuthNoc(inputUser, inputPass).then(function(res) {
              logger.debug(`${THISFILE}: ${res}`);
              driver = cas.getDriver();
              mainHandle = cas.getMainHandle();
              if (res) {
                ''
                submitIPMACPromise(inputAddr).then(function(ipCheck) {
                  if (ipCheck === true) {
                    getSolutionMessage(inputChoice).then(function(r) {
                      resolve(r);
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  getTestResults() {
    return testResults;
  }
}


/**
 * Submits an IP address in the form XXX.XXX.XXX.XXX or MAC Address in the
 * form X.X.X.X
 *
 * @return {boolean} [description]
 */
function submitIPMACPromise(ipMACInput) {
  logger.debug(`${THISFILE}: ${ipMACInput}`);
  return new Promise(function(resolve, reject) {

    var ipInput;
    var macInput;

    var regIP = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
    var regMAC = /([0-9A-Fa-f]{4}[.]){2}([0-9A-Fa-f]{4})/;

    var mac = ipMACInput.match(regMAC);
    var ip = ipMACInput.match(regIP);

    if (ip != null) {
      // Find the elements and store them into variables.
      driver.findElement(By.name("addr")).then(function(address) {
        ipInput = address;
      });
      driver.findElement(By.xpath("/html/body/p[2]/table" +
        "/tbody/tr[2]/td/form[1]/input[3]")).then(function(but) {

        // Run the submit operation
        ipInput.clear();
        logger.debug(`${THISFILE}: Sending IP: ${ipMACInput}`);
        ipInput.sendKeys(ipMACInput);
        but.click();

        driver.getAllWindowHandles().then(function(allHandles) {
          var winHandles = allHandles.slice();

          logger.debug(`${THISFILE}: You have ${winHandles.length} open.`);
          driver.switchTo().window(winHandles[1]);

          driver.getCurrentUrl().then(function(u) {
            if (!u.includes("netmgr/getdupe")) {
              // Process of elimination
              driver.switchTo().window(winHandles[0]);
            }
            driver.getCurrentUrl().then(function(checkURL) {
              if (u.includes("netmgr/getdupe")) {
                resolve(true); // Assumes we are able to change windows.
              } else {
                reject(false);
              }
            });
          });
        });
      });
    } else {
      // Find the elements and store them into variables.
      driver.findElement(By.name("mac")).then(function(address) {
        macInput = address;
      });
      driver.findElement(By.xpath("/html/body/p[2]/table" +
        "/tbody/tr[2]/td/form[2]/input[3]")).then(function(but) {
        ipButton = but;
        // Run the submit operation
        macInput.clear();
        logger.debug(`${THISFILE}: Sending MAC: ${ipMACInput}`);
        macInput.sendKeys(ipMACInput);
        but.click();

        driver.getAllWindowHandles().then(function(allHandles) {
          var winHandles = allHandles.slice();
          logger.debug(`${THISFILE}: ${winHandles}`);
          driver.switchTo().window(winHandles[1]);

          driver.getCurrentUrl().then(function(u) {
            if (!u.includes("netmgr/getdupe")) {
              // Process of elimination
              driver.switchTo().window(winHandles[0]);
            }
            driver.getCurrentUrl().then(function(checkURL) {
              if (u.includes("netmgr/getdupe")) {
                resolve(true); // Assumes we are able to change windows.
              } else {
                reject(false);
              }
            });
          });
        });
      });
    }
  });
}

/**
 * Returns the resolution message based on type. If user specifies a type of
 * 0, they will get a basic string. However if they select 1, they will get a
 * table which shows detailed info of when the IP was last seen in use or a
 * string if the IP isn't in use.
 *
 * @param  {Integer} type 0 or 1 for basic or detailed, respectively.
 * @return {String}      Returns a string which tells us status of IP
 */
function getSolutionMessage(type) {
  return new Promise(function(resolve, reject) {
    driver.getPageSource().then(function(src) {
      var $ = cheerio.load(src);
      var $2 = cheerio.load(src);

      var qString;

      $('p').each(function(i, el) {
        // this === el
        if (i == 1) {
          var s = $(this).text();
          qString = s.match(/\d+\/\d+\/\d{4}/);
        }
      })
      var stuff = $('table').last();

      var list = [];
      var mainList = [];

      stuff.find('td').each(function(i, ele) {
        list.push($(ele).text());
        if ((i + 1) % 4 === 0) {
          mainList.push(list);
          list = [];
        }
      });

      // 0 is basic and 1 is detailed.
      if (type == 0) {
        if (mainList.length == 0) {
          // driver.quit();
          testResults = '001';
          resolve(":recycle: **IP NOT IN USE**\n");
        } else if (mainList.length == 1) {
          // driver.quit();
          testResults = '001';
          resolve(":white_check_mark: **IP IN USE**\n");
        } else {
          // driver.quit();
          testResults = '001';
          resolve(`:warning: **IP IN USE.**
***(NOTE)** More than 1 MAC detected. Run detailed check by adding '-d' to your query for more info.*
`)
        }
        logger.debug(`${THISFILE}: Returned simple version of report`)
      } else if (type == 1) {
        if (mainList.length == 0) {
          testResults = '002';
          resolve(`:recycle:  ***This IP Has not been in use since ${qString}. This indicates that this IP can possibly be recycled for use on a new device***

`);
        } else {
          var messages = []
          for (var i = 0; i < mainList.length; i++) {
            messages.push("**__Address Resolution__**\n" +
              "**IP Address:** " + mainList[i][0] + "\n" +
              "**MAC Address:** " + mainList[i][1] + "\n" +
              "**First Seen:** " + mainList[i][2] + "\n" +
              "**Last Seen:** " + mainList[i][3] + "\n\n");
          }

          // If there is a conflict in IP assignment
          if (messages.length > 1) {
            messages.push(`:warning: ***This IP has been used by ${messages.length} machines since ${qString}. Investigate!!***

`)
          } else {
            messages.push(`:white_check_mark: ***This IP has been used by only ${messages.length} machine since ${qString}.***

`)
          }
          var formatMessages = '';
          for (var i = 0; i < messages.length; i++) {
            formatMessages += messages[i];
          }
          testResults = '002';
          resolve(formatMessages);
        }
        logger.debug(`${THISFILE}: Returned detailed version of report`)
      }
    });
  });
}
