// Log Manager - instantiated using npm package 'winston'.
const logger = require('./logging/manage/loggingManager.js');

// Discord Client requires these
var fs = require("fs-extra");
const Client = require('discord.js').Client;

// Grab env variables from config.
var config = JSON.parse(fs.readFileSync('./config/config.json', 'utf8'));
const token = config["JPSM_BOT"].bot_token;
const prefix = config["MY_GUILD"].guild_prefix;

// Create the bot.
const bot = new Client();

// Plugins to be attached to bot.
const general = require('./general/general.js');
const botLogger = require('./general/botLogger.js');
const guildListener = require('./general/guildListener.js');
const it_tasks = require('./it/it_tasks.js');
const rssServer = require('discord-rss');

// Apply the bot's logger plugin which logs the bot's state.
botLogger(bot, {
  prefix: `${prefix}`
});

// Starts the Bot then attach additional plugins.
bot.login(token).then(function(success) {

  // Apply the general plugin.
  general(bot, {
    prefix: `${prefix}`,
  });

  // Apply it tasks plugin.
  it_tasks(bot, {
    prefix: `${prefix}`,
    config: `${JSON.stringify(config)}`
  });

  // Apply the guildListener plugin.
  guildListener(bot, {
    prefix: `${prefix}`
  });
})
  .catch(function(err) {
    console.error(err);
  });
