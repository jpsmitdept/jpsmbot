// This instantiates the winston logger to be used by all other loggers.
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
require('winston-daily-rotate-file');
require('winston-loggly');

// Glabal Variables.
const tsFormat = () => (new Date()).toLocaleTimeString();

logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
  timestamp: tsFormat,
  colorize: true,
  level: 'silly'
});
logger.add(logger.transports.DailyRotateFile, {
  name: 'file#prod',
  timestamp: tsFormat,
  prettyPrint: true,
  filename: './logging/logs/prod/-production.log',
  datePattern: 'yyyy-MM-dd',
  prepend: true,
  level: 'info'
});
logger.add(logger.transports.DailyRotateFile, {
  name: 'file#dev',
  timestamp: tsFormat,
  prettyPrint: true,
  filename: './logging/logs/dev/-development.log',
  datePattern: 'yyyy-MM-dd',
  prepend: true,
  level: 'debug'
});
logger.add(logger.transports.Loggly, {
  name: 'loggly',
  subdomain: 'https://jpsm.loggly.com',
  inputToken: '305f15b6-c8f3-46be-9536-02501dad1a55',
  level: 'debug'
});

var disable = function() {
  logger.remove(logger.transports.Console);
  logger.remove('file#prod');
  logger.remove('file#dev');
  logger.remove('loggly');
}

module.exports = {
  logger,
  disable: disable
};
