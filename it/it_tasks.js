// Class logger, managed by loggingManager.js
const logger = require('winston');
const THISFILE = require('path').basename(__filename).toUpperCase();

// NPM Requirements.
const Timer = require('timer-machine');
const ping = require('ping');
const Repeat = require('repeat');
const Address4 = require('ip-address').Address4;

// Local Requirements.
const JPSMMailListener = require('../util/mail_listener.js');
const Noctools = require('../util/noctools.js');
const JPSMZipper = require('../util/jpsmzipper');
const Polycom = require('../util/polycom.js');
const Slack = require('../util/webhook_to_slack.js');
const Mailer = require('../util/send_mail.js');

/**
 * This is the IT main discord plugin.
 *
 * @param  {Client} bot     This the starting point Bot.
 * @param  {object} options Misc Options undefined for now
 * @return {Void}
 */
module.exports = function(bot, options) {
  logger.info(`${THISFILE}: Plugin Attached To Bot.`)

  let CONFIG = JSON.parse(options.config);
  let GUILD = CONFIG['MY_GUILD'];
  let CAS = CONFIG['CAS_CRED'];
  let ML = CONFIG['MAIL_LISTENER'];
  let SLACK = CONFIG["MY_GUILD"];
  let MAIL = CONFIG["MAIL_LISTENER"];
  let POLY = CONFIG['POLYCOM'];

  let allunits = POLY.allunits;
  let pword = POLY.password;

  // Listener objects
  var listenerInstance;

  // Class variables and objects
  var regIP = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
  var regMAC = /([0-9A-Fa-f]{4}[.]){2}([0-9A-Fa-f]{4})/;
  var zip = new JPSMZipper();
  var poly = new Polycom(pword, allunits);
  var noctools = new Noctools();
  var mail = new Mailer('gmail', MAIL.username, MAIL.password);

  // Pulls the custom prefix or defaults
  let PREFIX = (options && options.prefix) || ';';

  bot.on('message', msg => {
    const message = msg.content.trim();

    // Check if the message is a command.
    if (message.startsWith(PREFIX)) {
      // Get the command and suffix.
      const command = message.split(/[ \n]/)[0].substring(PREFIX.length).toLowerCase().trim();
      const suffix = message.substring(PREFIX.length + command.length).trim();

      // Process the commands.
      switch (command) {
        case 'checkmac':
        case 'maccheck':
        case 'checkip':
        case 'ipcheck':
          return ipcheck(msg, suffix);
        case 'pingmachine':
          return pingMachine(msg, suffix);
        case 'startlistener':
          return startListener(msg, suffix);
        case 'stoplistener':
          return stopListener(msg, suffix);
        case 'spammer':
          return spammer(msg, suffix);
        case 'polycom':
          return polycom(msg, suffix);
        case 'slack':
          return slack(msg, suffix);
        case 'sendmail':
          return sendmail(msg, suffix);
      }
    }
  });

  function polycom(msg, suffix) {
    var ip = suffix.match(regIP);
    var cmd = suffix;
    var unit;

    if (cmd.includes("2208")) {
      cmd = cmd.replace('2208', '');
      unit = POLY.unit3;
    } else if (cmd.includes("1218")) {
      cmd = cmd.replace('1218', '');
      unit = POLY.unit2;
    } else if (cmd.includes("1208")) {
      cmd = cmd.replace('1208', '');
      unit = POLY.unit1;
    } else {
      unit = suffix.split(" ")[0];
    }

    if ((cmd.includes("-d") && cmd.match(/(-d\w+)/) == null) ||
      cmd.includes("-dial")) { // Regular
      cmd = cmd.replace('-dial', '');
      cmd = cmd.replace('-d', '');
      cmd = cmd.trim();
      poly.dial(unit, cmd).then(function(sol) {

        msg.channel.sendMessage(`${msg.author}
**Dialing...**`)
          .then(function(message) {
            logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`);
            setTimeout(function() {
              poly.getCallState(unit).then(function(sol) {
                var connections = "";
                for (var i = 0; i < sol.length; i++) {
                  connections += `__*Connection #${i + 1}*__
*State: ${sol[i].state}
Speed: ${sol[i].speed}
Number: ${sol[i].dialstr}*
`;
                }
                if (sol[0].state === "connected") {
                  logger.info(`${THISFILE}: Polycom Unit Connected ${connections.replace(/(\r\n|\n|\r)/gm, "")}`);
                  msg.reply(`**:white_check_mark: CONNECTED!**
${connections}`)
                    .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
                    .catch(function(err) {
                      logger.error(`${THISFILE}: ${err}`);
                    });
                } else {
                  logger.info(`${THISFILE}: Polycom Unit Not Connected ${connections.replace(/(\r\n|\n|\r)/gm, "")}`);
                  msg.reply(`**:x: NOT CONNECTED!**
${connections}`)
                    .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
                    .catch(function(err) {
                      logger.error(`${THISFILE}: ${err}`);
                    });
                }
              });
            }, 20000);
          })
          .catch(err => logger.error(`${THISFILE}: ${err}`));
      });
    }
    if ((cmd.includes("-s") && cmd.match(/(-s\w+)/) == null) ||
      cmd.includes("-status")) {
      poly.getCallState(unit).then(function(sol) {
        var connections = "";
        for (var i = 0; i < sol.length; i++) {
          connections += `__*Connection #${i + 1}*__
*State: ${sol[i].state}
Speed: ${sol[i].speed}
Number: ${sol[i].dialstr}*
`;
        }
        if (sol[0].state === "connected") {
          logger.debug(`${THISFILE}: Connected ${connections.replace(/(\r\n|\n|\r)/gm, "")}`);
          msg.reply(`**:white_check_mark: CONNECTED!**
${connections}`)
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        } else {
          logger.debug(`${THISFILE}: Not Connected ${connections.replace(/(\r\n|\n|\r)/gm, "")}`);
          msg.reply(`**:x: NOT CONNECTED!**
${connections}`)
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        }
      });
    }
    if ((cmd.includes("-sn") && cmd.match(/(-sn\w+)/) == null) ||
      cmd.includes("-serialnumber")) {
      poly.getSN(unit).then(function(sol) {
        return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-n") && cmd.match(/(-n\w+)/) == null) ||
      cmd.includes("-name")) {
      poly.getSystemName(unit).then(function(sol) {
        return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-v") && cmd.match(/(-v\w+)/) == null) ||
      cmd.includes("-version")) {
      poly.getVersion(unit).then(function(sol) {
        return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-h") && cmd.match(/(-h\w+)/) == null) ||
      cmd.includes("-hangup")) {
      poly.hangup(unit).then(function(sol) {
        return msg.channel.sendMessage(msg.author +
          "\n" + "**Call Dropped**")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-r") && cmd.match(/(-r\w+)/) == null) ||
      cmd.includes("-restart")) {
      if (cmd.includes("all")) {
        poly.restartAll().then(function(sol) {
          return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      } else {
        poly.restart(unit).then(function(sol) {
          return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      }
    }
    if ((cmd.includes("-i") && cmd.match(/(-i\w+)/) == null) ||
      cmd.includes("-info")) {
      poly.whoami(unit).then(function(sol) {
        return msg.channel.sendMessage(`${msg.author}
**${sol}**`)
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-si") && cmd.match(/(-si\w+)/) == null) ||
      cmd.includes("-setinstructor")) {
      poly.setCamInstructor(unit).then(function(sol) {
        return msg.channel.sendMessage(msg.author +
          "\n" + "**Camera Set: Instructor**")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
    if ((cmd.includes("-ss") && cmd.match(/(-ss\w+)/) == null) ||
      cmd.includes("-setstudents")) {
      poly.setCamStudents(unit).then(function(sol) {
        return msg.channel.sendMessage(msg.author +
          "\n" + "**Camera Set: Students**")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
  }

  /**
   * Returns information about the server the message was sent in.
   * @param  {String} msg    [description]
   * @param  {String} suffix [description]
   * @return {[type]}        [description]
   */
  function ipcheck(msg, suffix) {
    var myTimer = new Timer();
    myTimer.start();

    var mac = suffix.match(regMAC);
    var ip = suffix.match(regIP);

    // Start Typing animation in discord
    msg.channel.startTyping();

    if (mac == null && ip == null) {
      msg.channel.stopTyping();
      myTimer.stop();
      return msg.channel.sendMessage(msg.author +
        "\n" + ":no_entry_sign: **Please Enter IPv4 in form X.X.X.X or MAC in form X.X.X**")
        .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    }

    if (ip != null) {
      var address = new Address4(ip[0]);
      if (!address.isValid()) {
        msg.channel.stopTyping();
        myTimer.stop();
        return msg.channel.sendMessage(msg.author +
          "\n" +
          ":no_entry_sign: **Please Enter IPv4 in form X.X.X.X or MAC in form X.X.X**")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      }
      if (suffix.includes("-d")) {
        noctools.query(CAS.username, CAS.password, 1, ip[0]).then(function(sol) {
          // logger.debug(sol);
          myTimer.stop();
          msg.channel.stopTyping();
          msg.channel.sendMessage(msg.author +
            "\n" + sol + "*Query finished in " +
            (myTimer.time() / 1000) + " seconds.*")
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      } else {
        noctools.query(CAS.username, CAS.password, 0, ip[0]).then(function(sol) {
          myTimer.stop();
          msg.channel.stopTyping();
          msg.channel.sendMessage(msg.author + "\n" + sol + "*Query finished in " + (myTimer.time() / 1000) + " seconds.*")
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      }

    } else if (mac != null) {
      if (suffix.includes("-d")) {
        noctools.query(CAS.username, CAS.password, 1, mac[0]).then(function(sol) {
          myTimer.stop();
          msg.channel.stopTyping();
          msg.channel.sendMessage(msg.author + "\n" + sol + "*Query finished in " + (myTimer.time() / 1000) + " seconds.*")
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      } else {
        noctools.query(CAS.username, CAS.password, 0, mac[0]).then(function(sol) {
          myTimer.stop();
          msg.channel.stopTyping();
          msg.channel.sendMessage(msg.author + "\n" + sol + "*Query finished in " + (myTimer.time() / 1000) + " seconds.*")
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      }
    }
  }

  /**
   * This pings a machine's IP address.
   *
   * @param  {String} msg    [description]
   * @param  {String} suffix [description]
   * @return {[type]}        [description]
   */
  function pingMachine(msg, suffix) {
    var myTimer = new Timer();
    var ip = suffix.match(regIP);

    myTimer.start();
    msg.channel.startTyping();
    if (ip == null) {
      msg.channel.stopTyping();
      myTimer.stop();
      return msg.channel.sendMessage(msg.author + "\n" + ":no_entry_sign: **Please Enter IPv4 in form X.X.X.X**")
        .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
        .catch(function(err) {
          logger.error(`${THISFILE}: ${err}`);
        });
    }

    if (ip != null) {
      var address = new Address4(ip[0]);
      if (!address.isValid()) {
        msg.channel.stopTyping();
        myTimer.stop();
        return msg.channel.sendMessage(msg.author + "\n" + ":no_entry_sign: **Please Enter IPv4 in form X.X.X.X**")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      }

      ping.sys.probe(ip[0], function(isAlive) {
        myTimer.stop();
        msg.channel.stopTyping();
        var sol = isAlive ? ':white_check_mark: **Host: ' + ip[0] + ' is alive**' : ':no_entry_sign: **Host ' + ip[0] + ' is dead**';
        msg.channel.sendMessage(msg.author + "\n" + sol + "\n\n*Query finished in " + (myTimer.time() / 1000) + " seconds.*")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      });
    }
  }

  /**
   * This starts the mail listener.
   *
   * @param  {String} msg    [description]
   * @param  {String} suffix [description]
   * @return {[type]}        [description]
   */
  (function startListener(msg, suffix) {
    bot.guilds.get(GUILD.id)
      .channels.find('name', GUILD.mail_channel_name)
      .sendMessage(`@everyone
**Mail Listener Started.**`)
      .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
      .catch(function(err) {
        logger.error(`${THISFILE}: ${err}`);
      });

    listenerInstance = new JPSMMailListener(
      ML.username, ML.password, ML.imap, ML.port, ML.inbox, true);
    mailToDiscord();
  })();

  /**
   * This starts the mail listener.
   *
   * @param  {String} msg    [description]
   * @param  {String} suffix [description]
   * @return {[type]}        [description]
   */
  function stopListener(msg, suffix) {
    msg.channel.sendMessage(`${msg.author}
**Mail Listener Stopped.**`)
      .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
      .catch(function(err) {
        logger.error(`${THISFILE}: ${err}`);
      });
    listenerInstance.stopListener
  }

  /**
   * This starts the mail listener.
   *
   * @param  {String} msg    [description]
   * @param  {String} suffix [description]
   * @return {[type]}        [description]
   */
  function mailToDiscord(msg, suffix) {
    var onSuccess = function(results) {
      logger.debug(`${THISFILE}: All goods ${results}`);
    };

    var onFailure = function(exception) {
      logger.error(`${THISFILE}: Mailer-Error ${exception}`);
    };

    var onProgress = function(result) {
      if (result != undefined) {
        logger.debug(`${THISFILE}: You've Got ${result.length} New Mail`);
        logger.debug(`${THISFILE}: Logging Results`);
        for (var i = 0; i < result.length; i++) {
          sendToDiscordChannel(bot, GUILD.id,
            'mail-listener', result[i].message, {
              attachmentDir: result[i].attachment_dir
            });
        }
      }
    };

    Repeat(listenerInstance.checkMail)
      .every(5, 's')
      .start.in(10, 's')
      .then(onSuccess, onFailure, onProgress);
  }

  function spammer(msg, suffix) {
    sendToDiscordChannel(bot, GUILD.id, 'general', suffix, {
      attachmentDir: "./util/attachments/Pallavi Drawing.pdf",
    })
  }

  /**
   * Handles messages which could exceed 2000 characters by splitting them into
   * seperate parts. And posts said message to channel.
   *
   * @param  Client dBot    [description]
   * @param  String gID     [description]
   * @param  String cName   [description]
   * @param  String sstring [description]
   * @return {[type]}         [description]
   */
  function sendToDiscordChannel(dBot, gID, cName, sstring, options) {
    var options = options || {};
    var attachmentDir = options.attachmentDir || 'none';
    logger.debug(`${THISFILE}: File name is : ${attachmentDir}`);

    if (attachmentDir === '' || attachmentDir === 'none') {
      attachmentDir = 'none';
    }

    logger.debug(`${THISFILE}: Length of sstring: ${sstring.length}`);
    if (attachmentDir === 'none') {
      if (sstring.length < 1990) {
        dBot.guilds.get(gID)
          .channels.find('name', cName)
          .sendMessage("@everyone\n" + sstring)
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      } else {
        var tempString = sstring.substring(0, 1985);
        dBot.guilds.get(gID)
          .channels.find('name', cName)
          .sendMessage("@everyone\n" + tempString + "....")
          .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
          .catch(function(err) {
            logger.error(`${THISFILE}: ${err}`);
          });
      }
    } else {
      if (sstring.length < 1990) {
        logger.debug(`${THISFILE}: Trying to grab ${attachmentDir}.zip`);
        zip.directory(attachmentDir, `${attachmentDir}.zip`).then(function(r) {
          dBot.guilds.get(gID)
            .channels.find('name', cName)
            .send("@everyone\n" + sstring, {
              file: `${attachmentDir}.zip`,
            })
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      } else {
        logger.debug(`${THISFILE}: Trying to grab ${attachmentDir}.zip`);
        zip.directory(attachmentDir, `${attachmentDir}.zip`).then(function(r) {
          var tempString = sstring.substring(0, 1985);
          dBot.guilds.get(gID)
            .channels.find('name', cName)
            .send("@everyone\n" + tempString + "....", {
              file: `${attachmentDir}.zip`,
            })
            .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
            .catch(function(err) {
              logger.error(`${THISFILE}: ${err}`);
            });
        });
      }
    }
  }

  function slack(msg, suffix) {
    var slack = new Slack(SLACK.slack_out);
    slack.send(suffix);

    return msg.channel.sendMessage(`**Message Sent To Slack!**`)
      .then(message => logger.debug(`${THISFILE}: Discord Message Sent [ Channel: ${message.channel.name} Author: ${message.author} ID: ${message.id} ].`))
      .catch(function(err) {
        logger.error(`${THISFILE}: ${err}`);
      });
  }

  function sendmail(msg, suffix) {
    if ( (cmd.includes("-to") && cmd.match(/(-i\w+)/) == null) ) {
    }

    mail.send('Test Message',
      'This is the message',
      'JPSM IT', MAIL.username,
      'edwidgefabre@gmail.com');
  }
}
